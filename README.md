# Docker Workshop
Lab 02: Basic commands

---

## Notes

 - During the lab we will use the following image:
 
https://hub.docker.com/r/selaworkshops/npm-static-app/

 - This image contain a basic web application


## Instructions

 - Create a new container from the image without running it:
```
$ docker create --name app-stopped selaworkshops/npm-static-app:latest
```

 - Create and run a new container (in detached mode) using:
```
$ docker run -d -p 3000:3000 --name app-running selaworkshops/npm-static-app:latest
```

 - Check that the second container is up and running:
```
$ docker ps
```

 - Check that the first container was created but is not running:
```
$ docker ps -a
```

 - Browse to the running container application:
```
http://server-ip:3000
```

 - Check which images exist in your host:
```
$ docker images
```

 - Remove both containers (with the force flag):
```
$ docker rm -f app-running app-stopped
```

 - Remove the containers image:
```
$ docker rmi selaworkshops/npm-static-app:latest
```

 - Check the containers status:
```
$ docker ps -a
```

 - Check the images status:
```
$ docker images
```
